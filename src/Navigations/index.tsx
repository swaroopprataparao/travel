import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { About, Contact, Home, Projects, Skills } from '../containers';

const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator initialRouteName='Home' screenOptions={{
      headerShown: false
    }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="About" component={About} />
      <Tab.Screen name="Skills" component={Skills} />
      <Tab.Screen name="Contact" component={Contact} />
      <Tab.Screen name="Projects" component={Projects} />
    </Tab.Navigator>
  );
}

export default MyTabs