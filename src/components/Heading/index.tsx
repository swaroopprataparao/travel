import { View, Text, ViewStyle } from 'react-native'
import React from 'react'
import { styles } from './styles'

interface Props {
  heading: string;
  customStyles?: ViewStyle
}

const Heading = ({heading, customStyles}: Props) => {
  return (
    <>
      <Text style={[styles.title, customStyles]}>{heading}</Text>
    </>
  )
}

export default Heading