import {StyleSheet} from 'react-native';
import {COLORS} from '../../utils/Colors';

export const styles = StyleSheet.create({
  title: {
    color: COLORS.primary,
    fontWeight: 'bold',
    fontSize: 32,
    marginTop: 25,
  },
});
