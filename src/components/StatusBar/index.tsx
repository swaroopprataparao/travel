import { View, Text } from 'react-native'
import React from 'react'
import { COLORS } from '../../utils/Colors'
import { styles } from './styles'

const StatusBar = () => {
  return (
    <View style={styles.container} />
  )
}

export default StatusBar