import { StyleSheet } from "react-native";
import { COLORS } from "../../utils/Colors";

export const styles = StyleSheet.create({
  container: {height: 50, width: '100%', backgroundColor: COLORS.primary}
})