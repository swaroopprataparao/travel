export const COLORS = {
  primary: '#1C1052',
  secondary: '#A6ADAE',
  offWhite: '#D9D9D9'
}