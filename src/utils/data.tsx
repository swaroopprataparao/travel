export const data = {
  content:
    'Versatile Developer with over 3 years of leveraging software engineering and implementing new technologies to maximize development efficiency. Conducting extensive market and consumer research to translate into applications features. Acquiring and applying new technology concepts in short period of time.',
  about:
    "My name is Swaroop Prataparo. I specalise in creating beautiful, usable, Professional applications using best practice accessibility. For building apps my weapon is React Native. All my applications are lovingly hand coded. I'm a freelance mobile app developer and pursuing masters in computer science with 2+ years of experience. I’ve done 7 projects and I enjoy making them to be user-friendly, elegant and eye-catching. To find out more about what I can do for you., Check out my work, or get a hold of me by either email.",
  skills:
    "I value business or brand for which i'm creating, thus i enjoy bringing new ideas to life.",
  projects: [
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
      technologies: ['React Native', 'TypeScript', 'Node JS', 'React Native', 'TypeScript', 'Node JS']
    },
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
    },
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
    },
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
    },
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
    },
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
    },
    {
      name: 'WorldSchool',
      description: 'This is an online teaching app which was designed for both iOS and android. The application consists of chat, appointments, search classes by user etc. A teacher can add classes either in public or private and invite students to respective classes and a user can search classes by entering username or name of the particular teacher. The public classes can be visible to all the users and the users can join in the class without any restrictions',
    },
  ],
};

export const SKILLS = [
  'HTML5',
  'CSS3',
  'JAVASCRIPT',
  'TYPESCRIPT',
  'REACT NATIVE',
  'REACT JS',
  'GIT',
  'JIRA',
  'GITLAB',
];
