import {View, Text, Image} from 'react-native';
import React from 'react';
import {data} from '../../utils/data';
import { Heading } from '../../components';
import {styles} from './styles';

const About = () => {
  return (
    <>
      <View style={styles.container}>
        <Heading heading='About Me' />
        <View style={styles.imageWrapper}>
          <Image
            source={require('../../Assets/profile.png')}
            style={styles.image}
          />
        </View>

        <View style={styles.contentWrapper}>
          <Text style={styles.content}>{data.about}</Text>
        </View>
      </View>
    </>
  );
};

export default About;
