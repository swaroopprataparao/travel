import { StyleSheet } from "react-native";
import { COLORS } from "../../utils/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.secondary,
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  heading: {
    color: COLORS.primary,
    fontWeight: 'bold',
    fontSize: 32,
    padding: 20,
  },
  imageWrapper: {
    height: 110,
    width: 110,
    backgroundColor: COLORS.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 110,
    marginTop: 20
  },
  image: {height: 100, width: 100, marginBottom: 20},
  contentWrapper: {
    backgroundColor: COLORS.offWhite,
    padding: 18,
    marginTop: 20,
    borderRadius: 10,
    height: 350
  },
  content: {color: COLORS.primary, fontWeight: 'bold', fontSize: 16, marginTop: 10}
})