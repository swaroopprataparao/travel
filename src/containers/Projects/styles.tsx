import { StyleSheet } from "react-native";
import { COLORS } from "../../utils/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.secondary,
    paddingHorizontal: 20,
  },
  nameContainer: {
    height: 60,
    backgroundColor: COLORS.primary,
    justifyContent: 'center',
    paddingLeft: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  name: {fontWeight: 'bold', color: COLORS.offWhite, fontSize: 18},
  descriptionContainer: {
    padding: 10,
    backgroundColor: COLORS.offWhite,
    marginBottom: 15,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    paddingVertical: 20,
  },
  description: {color: COLORS.primary},
  techContainer: {
    backgroundColor: COLORS.primary,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginHorizontal: 5
  },
  technologies: {color: COLORS.offWhite, fontWeight: 'bold'},
  topMargin: { marginTop: 15}
})