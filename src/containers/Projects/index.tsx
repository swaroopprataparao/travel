import {View, Text, FlatList, ScrollView} from 'react-native';
import React from 'react';
import {Heading} from '../../components';
import {data} from '../../utils/data';
import {styles} from './styles';

interface Props {
  item: any;
  index: number;
}

const Information = ({item, index}: Props) => {
  return (
    <>
      <View key={index} style={styles.nameContainer}>
        <Text style={styles.name}>{item.name}</Text>
      </View>

      <View style={styles.descriptionContainer}>
        <Text style={styles.description}>{item.description}</Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={styles.topMargin}>
          {item.technologies?.map((tech: string[], index: number) => (
            <View key={index} style={styles.techContainer}>
              <Text style={styles.technologies}>{tech}</Text>
            </View>
          ))}
        </ScrollView>
      </View>
    </>
  );
};

const Projects = () => {
  return (
    <View style={styles.container}>
      <Heading heading="Projects" customStyles={{marginBottom: 20}} />
      <FlatList
        data={data.projects}
        showsVerticalScrollIndicator={false}
        renderItem={({item, index}) => (
          <Information item={item} index={index} />
        )}
      />
    </View>
  );
};

export default Projects;
