import { StyleSheet } from "react-native";
import { COLORS } from "../../utils/Colors";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.secondary,
    paddingHorizontal: 20,
  },
  text: {
    fontWeight: 'bold',
    marginTop: 10,
    fontSize: 16,
    color: '#000',
    marginBottom: 15,
  },
  card: { 
    height: 100,
    width: 125,
    margin: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: COLORS.offWhite,
    borderRadius: 10,
  },
  circle: {height: 35, width: 35, backgroundColor: COLORS.primary, borderRadius: 35},
  skills: {fontWeight: 'bold', color: COLORS.primary}
})