import {View, Text, FlatList} from 'react-native';
import React from 'react';
import {Heading} from '../../components';
import {data, SKILLS} from '../../utils/data';
import {styles} from './styles';

const Skills = () => {
  return (
    <>
      <View style={styles.container}>
        <Heading heading="Skills" />
        <Text style={styles.text}>{data.skills}</Text>
        <FlatList
          data={SKILLS}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}) => (
            <View style={{flex: 1}}>
              <View key={index} style={styles.card}>
                <View style={styles.circle} />
                <Text style={styles.skills}>{item}</Text>
              </View>
            </View>
          )}
        />
      </View>
    </>
  );
};

export default Skills;
