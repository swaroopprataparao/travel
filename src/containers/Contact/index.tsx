import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import React from 'react'
import { COLORS } from '../../utils/Colors'
import { Heading } from '../../components'
import { styles } from './styles'

const Contact = () => {
  return (
    <View style={styles.container}>
      <Heading heading='Contact' customStyles={{marginBottom: 50}} />
      <View style={styles.inputWrapper}>
        <TextInput placeholder='Name' placeholderTextColor={COLORS.primary} style={styles.input} />
        <TextInput placeholder='Email' placeholderTextColor={COLORS.primary} style={styles.input} />
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Contact