import { StyleSheet } from "react-native";
import { COLORS } from "../../utils/Colors";

export const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: COLORS.secondary, paddingHorizontal: 20},
  inputWrapper: {flex: 0.5, backgroundColor: COLORS.secondary},
  input: {borderWidth: 1, fontWeight: 'bold', fontSize: 16, paddingLeft: 10, marginBottom: 10, borderRadius: 10},
  button: {backgroundColor: COLORS.primary, padding: 10, borderRadius: 10, justifyContent: 'center', alignItems: 'center'},
  buttonText: {color: COLORS.offWhite, fontWeight: 'bold'}
})