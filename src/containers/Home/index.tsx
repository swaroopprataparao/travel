import {View, Text, Image} from 'react-native';
import React from 'react';
import {styles} from './styles';
import {data} from '../../utils/data';
import { Heading } from '../../components';

const Home = () => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.description}>
          <Text style={styles.title}>Prataparao</Text>
          <Text style={styles.subTitle}>Swaroop</Text>
          <Text style={styles.content}>{data.content}</Text>
        </View>

        <View style={styles.imageWrapper}>
          <Image
            source={require('../../Assets/profile.png')}
            style={styles.image}
          />
        </View>
      </View>
    </>
  );
};

export default Home;
