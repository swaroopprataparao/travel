import { StyleSheet } from "react-native";
import { COLORS } from "../../utils/Colors";

export const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: COLORS.secondary},
  description: {flex: 0.4, padding: 25},
  title: {color: COLORS.primary, fontWeight: 'bold', fontSize: 32, marginTop: 25},
  subTitle: {color: COLORS.primary, fontWeight: 'bold', fontSize: 32, marginBottom: 20},
  content: {fontSize: 16, color: COLORS.primary},
  imageWrapper: {flex: 0.6},
  image: { right: 50, bottom: 50 }
})