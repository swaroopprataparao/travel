import React, {useState, useEffect} from 'react';
import {Text, Image, StyleSheet} from 'react-native';
import AnimatedSplash from 'react-native-animated-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import MyTabs from './src/Navigations';
import {COLORS} from './src/utils/Colors';

const App = () => {
  const [hideSplash, setHideSplash] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setHideSplash(true);
    }, 1000);
  }, []);

  useEffect(() => {
    hideSplash;
  }, [hideSplash]);

  const customComponent = () => {
    return (
      <>
        <Image
          source={require('./src/Assets/profile.png')}
          style={styles.image}
        />
        <Text style={styles.text}>Welcome !</Text>
      </>
    );
  };

  return (
    <NavigationContainer>
      <AnimatedSplash
        translucent={false}
        isLoaded={hideSplash}
        customComponent={customComponent()}
        backgroundColor={COLORS.primary}>
        <MyTabs />
      </AnimatedSplash>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({
  image: {height: 250, width: 250, marginVertical: 25},
  text: {
    color: COLORS.offWhite,
    fontSize: 30,
    fontWeight: 'bold',
    width: '100%',
  },
});
